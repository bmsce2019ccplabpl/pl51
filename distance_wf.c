#include <stdio.h>
#include <math.h>
 
float distance(float x1, float x2, float y1, float y2)
{
    float distance;
    distance = sqrt((x1 - y1) * (x1 - y1) + (x2 - y2) * (x2 - y2));
    return distance;
}
 
int main()
{
    float result,x1,y1,x2,y2;
    printf("Enter the coordinates of x1,y1,x2,y2:\n");
    scanf("%f%f%f%f",&x1,&y1,&x2,&y2);
    result = distance(x1,y1,x2,y2);
    printf("The distance between the points is %f", result);
    return 0;
}