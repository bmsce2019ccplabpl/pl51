#include <stdio.h>
int main()
{
    char a;
    FILE *fp;
    fp = fopen("input.txt","w");
    printf("Enter the contents to be written in  the file:\n");
    while((a = getchar())!= EOF)
    {
        fputc(a,fp);
    }
    fclose(fp);
    fp = fopen("input.txt","r");
    printf("The contents of the file are :\n");
    while((a = fgetc(fp)) != EOF)
        printf("%c",a);
    fclose(fp);
    return 0;
}