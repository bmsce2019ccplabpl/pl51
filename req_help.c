#include <stdio.h>
#include <math.h>
float distance(float x1, float x2, float y1, float y2);
float input(float x1,float y1,float x2,float y2);
void output(float d);
int main()
{
  float result,x1,y1,x2,y2,d;
  input(x1,y1,x2,y2);
  distance(x1,y1,x2,y2);
  output(d);
  return 0;
}

float input(float x1, float x2, float y1, float y2)
{
  printf("Enter the coordinates of x1,y1,x2,y2:\n");
  scanf("%f%f%f%f",&x1,&y1,&x2,&y2);
  return x1,y1,x2,y2;
}

float distance(float x1, float x2, float y1, float y2)
{
  float d;
  d = sqrt((x1 - y1) * (x1 - y1) + (x2 - y2) * (x2 - y2));
  return d;
}

void output(float d)
{
  printf("The distance between the points is %f", d);
}