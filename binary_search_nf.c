#include<stdio.h>
int main()
{
    int n,mid,f = 0,k,beg = 0;
    printf("Enter the total number of elements of the array\n");
    scanf("%d",&n);
    int a[n],end = n - 1;
    printf("Enter the elements of the array\n");
    
    for(int i = 0;i < n;i++)
    {
        scanf("%d",&a[i]);
    }
    
    printf("Enter the number to be found\n");
    scanf("%d",&k);
    
    while(beg <= end)
    {
        mid = (beg + end)*0.5;
        if(a[mid] == k)
        {
            f = 1;
            printf("Element %d is at %d position\n",k,mid + 1);
            break;
        }
        
        else if(a[mid] < k)
            beg = mid + 1;
        
        else
            end = mid - 1;
    }
    
    if(f == 0 && beg > end)
        printf("Element not found\n");
    
    return 0;
    
}