#include<stdio.h>

int input(int hours,int mins);

int convert(int hours,int mins);

void output(int total);

int main()
{
  int hours,mins,total;
  
  input(hours,mins);
  
  return 0;
}

int input(int hours,int mins)
{
  printf("Enter the hours and minutes to be calculated\n");
  
  scanf("%d%d",&hours,&mins);
  
  convert(hours,mins);
}
 
int convert(int hours,int mins)
{
   int total;
   
   total = (hours*60) + mins;
   
   output(total);
}

void output(int total)
{
  printf("The total number of minutes = %d\n",total);
}