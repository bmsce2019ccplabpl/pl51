#include <stdio.h>

int input(int n);

int compute(int n);

void output(int o,int t);

int main()

{
  int n;
  input(n);
}

int input(int n)

{
  printf("Enter any integer\n");
  
  scanf("%d",&n);
  
  compute(n);
}

int compute(int n)

{
  int o,m,t = 0;
  
  o = n;
  
  while(n != 0)
  
  {
    m = n % 10;
    
    t = (t * 10) + m;
    
    n = n / 10;
  }
  
  output(o,t);
  
}

void output(int o,int t)

{
  
  if(o == t)
  
    printf("%d is a palindrome\n",o);
  
  else
  
    printf("%d is not a palindrome\n",o);

  printf("The reversed number is %d\n",t);
}