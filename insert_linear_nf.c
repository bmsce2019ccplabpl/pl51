#include<stdio.h>
int main()
{
    int n,pos,num;
    printf("Enter the total number of elements\n");
    scanf("%d",&n);
    int a[n];
    printf("Enter the elements\n");
    
    for(int i = 0;i < n; i++)
    {
        scanf("%d",&a[i]);
    }
    
    printf("Enter the number to be inserted\n");
    scanf("%d",&num);
    
    printf("Enter the position at which %d is to be inserted\n",num);
    scanf("%d",&pos);
    
    for(int j = n - 1;j >= pos; j--)
    {
        a[j] = a[j + 1];
    }
    
    a[pos] = num;
    n = n + 1;
    
    printf("The values are \n");
    
    for(int k = 0; k < n;k++)
    {
        printf("%d\n",a[k]);
    }
    
    return 0;
}