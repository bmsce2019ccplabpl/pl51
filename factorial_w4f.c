#include <stdio.h>

int input(int n);

int compute(int n);

void output(int n,int f);

int main()

{
  int n,f;
  
  input(n);
}

int input(int n)

{
  printf("Enter the number whose factorial is to be found\n");
  
  scanf("%d",&n);
  
  compute(n);
}

int compute(int n)

{
  int f = 1,c = 1;
  
  while(c <= n)
  {
   f = f * c;
   
   c++;
  }
  
  output(n,f);
}

void output(int n,int f)

{
  printf("The factorial of %d is %d\n",n,f);  
}