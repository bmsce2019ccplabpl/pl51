#include <stdio.h>
#include <math.h>
float distance(float x1, float x2, float y1, float y2);
void input_point(float *x, float *y);
void output(float d);
int main()
{
  float x1,y1,x2,y2,d;
  input_point(&x1,&y1);
  input_point(&x2,&y2);
  d=distance(x1,x2,y1,y2);
  output(d);
  return 0;
}

void input_point( float *x, float *y)
{ 
    float a,b;
  printf("Enter the coordinates:\n");
  scanf("%f %f",&a,&b);
  *x=a;
  *y=b;
}

float distance(float x1, float x2, float y1, float y2)
{
  float d;
  d = sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
  return d;
}

void output(float d)
{
  printf("The distance between the points is %f", d);
}