#include <stdio.h>
int main()
{
    int m,n,i,sum = 0;
    printf("Enter the number whose digits are to be added\n");
    scanf("%d",&n);
    i = n;
    while(n > 0)
    {
        m = n % 10;
        sum = sum + m;
        n = n / 10;
    }
    
    printf("The sum of the digits of %d is %d\n",i,sum);
    return 0;
}
