#include <stdio.h>
int input(int a,int b);
void compute(int a,int b);
void output(int sum,int difference,int product,float quotient);
int main()
{
  int a,b;
  input(a,b);
	return 0;
}

int input(int a,int b)
{
  printf("Enter two numbers a and b\n");
  scanf("%d%d",&a,&b);
  compute(a,b);
}

void compute(int a,int b)
{
  int sum,difference,product;
  float quotient;
  sum = a + b;
  difference = a - b;
  product = a * b;
  quotient = (float) a / b;
  output(sum,difference,product,quotient);
}

void output(int sum,int difference,int product,float quotient)
{
  printf("The sum is %d\n" ,sum);
  printf("The difference is %d\n" ,difference);
  printf("The product is %d\n" ,product);
  printf("The quotient is %f\n" ,quotient);
}