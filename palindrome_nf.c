#include <stdio.h>

int main()
{
    int n,t = 0,m,o;
    printf("Enter any integer\n");
    scanf("%d",&n);
    o = n;
    
    while(n != 0)
    {
        m = n % 10;
        t = (t * 10) + m;
        n = n / 10;
    }
    
    if(o == t)
        printf("%d is a palindrome\n",o);
    else
        printf("%d is not a palindrome\n",o);
    
    printf("The reversed number is %d\n",t);
    return 0;
}