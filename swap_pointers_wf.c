#include <stdio.h>
void swap(int *x,int *y);
int main()
{
    int a,b;
    printf("Enter the numbers to be swapped:\n");
    scanf("%d%d",&a,&b);
    swap(&a,&b);
    printf("After interchanging: %d %d\n",a,b);
    return 0;    
}

void swap(int *x,int *y)
{
    int t;
    t = *x;
    *x = *y;
    *y = t;
}