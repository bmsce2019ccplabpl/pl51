#include <stdio.h>

int main()
{
  int n,k,c;
  printf("Enter any integer to convert into binary\n");
  scanf("%d",&n);
  for (c = 31;c >= 0;c--)
  {
      k = n >> c;
      
      if (k == 1)
          printf("1");
      else
          printf("0");
  }
  return 0;
}