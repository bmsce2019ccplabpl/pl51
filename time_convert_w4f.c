#include <stdio.h>
int input(int h,int m);
int compute(int h,int m);
void output(int t);
int main()
{
    int h,m;
    input(h,m);
    return 0;
}

int input(int h,int m)
{
  printf("Enter the time in hours and minutes\n");
  scanf("%d%d",&h,&m);
  compute(h,m);
}

int compute(int h,int m)
{
  int t;
  t = m + (h * 60);
  output(t);
}

void output(int t)
{
  printf("Time in mins is:%d",t);
}