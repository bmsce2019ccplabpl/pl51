#include <stdio.h>

int compute(int n);

int main()

{
  int n,o,t;
  
  printf("Enter any integer\n");
  
  scanf("%d",&n);
  
  compute(n);
  
  return 0;
  
}

int compute(int n)

{
  int o,m,t = 0;
  
  o = n;
  
  while(n != 0)
  
  {
    m = n % 10;
    
    t = (t * 10) + m;
    
    n = n / 10;
  }
  
  if(o == t)
  
   printf("%d is a palindrome\n",o);
  
  else
  
   printf("%d is not a palindrome\n",o);

  printf("The reversed number is %d\n",t);

}