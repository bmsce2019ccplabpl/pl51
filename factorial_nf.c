#include <stdio.h>
int main()
{
    int n,f = 1,c = 1;
    printf("Enter the number whose factorial is to be found\n");
    scanf("%d",&n);
    
    while(c <= n)
    {
        f = f * c;
        c++;
    }
    
    printf("The factorial of %d is %d\n",n,f);
    return 0;
}